def calculate_next_step(surface, mesh_height, mesh_width):
    new_surface = [[False for x in range(mesh_width)] for y in range(mesh_height)]

    for i in range(mesh_height):
        for j in range(mesh_width):
            alive = 0

            if i == 0:
                if j == 0:

                    if surface[mesh_height - 1][mesh_width - 1]:
                        alive += 1
                    if surface[mesh_height - 1][j]:
                        alive += 1
                    if surface[mesh_height - 1][j + 1]:
                        alive += 1
                    if surface[i][mesh_width - 1]:
                        alive += 1
                    if surface[i][j + 1]:
                        alive += 1
                    if surface[i + 1][mesh_width - 1]:
                        alive += 1
                    if surface[i + 1][j]:
                        alive += 1
                    if surface[i + 1][j + 1]:
                        alive += 1

                elif j == mesh_width - 1:

                    if surface[mesh_height - 1][j - 1]:
                        alive += 1
                    if surface[mesh_height - 1][j]:
                        alive += 1
                    if surface[mesh_height - 1][0]:
                        alive += 1
                    if surface[i][j - 1]:
                        alive += 1
                    if surface[i][0]:
                        alive += 1
                    if surface[i + 1][j - 1]:
                        alive += 1
                    if surface[i + 1][j]:
                        alive += 1
                    if surface[i + 1][0]:
                        alive += 1
                else:
                    if surface[mesh_height - 1][j - 1]:
                        alive += 1
                    if surface[mesh_height - 1][j]:
                        alive += 1
                    if surface[mesh_height - 1][j + 1]:
                        alive += 1
                    if surface[i][j - 1]:
                        alive += 1
                    if surface[i][j + 1]:
                        alive += 1
                    if surface[i + 1][j - 1]:
                        alive += 1
                    if surface[i + 1][j]:
                        alive += 1
                    if surface[i + 1][j + 1]:
                        alive += 1

            elif i == mesh_height - 1:

                if j == 0:

                    if surface[i - 1][mesh_width - 1]:
                        alive += 1
                    if surface[i - 1][j]:
                        alive += 1
                    if surface[i - 1][j + 1]:
                        alive += 1
                    if surface[i][mesh_width - 1]:
                        alive += 1
                    if surface[i][j + 1]:
                        alive += 1
                    if surface[0][mesh_width - 1]:
                        alive += 1
                    if surface[0][j]:
                        alive += 1
                    if surface[0][j + 1]:
                        alive += 1

                elif j == mesh_width - 1:

                    if surface[i - 1][j - 1]:
                        alive += 1
                    if surface[i - 1][j]:
                        alive += 1
                    if surface[i - 1][0]:
                        alive += 1
                    if surface[i][j - 1]:
                        alive += 1
                    if surface[i][0]:
                        alive += 1
                    if surface[0][j - 1]:
                        alive += 1
                    if surface[0][j]:
                        alive += 1
                    if surface[0][0]:
                        alive += 1
                else:

                    if surface[i - 1][j - 1]:
                        alive += 1
                    if surface[i - 1][j]:
                        alive += 1
                    if surface[i - 1][j + 1]:
                        alive += 1
                    if surface[i][j - 1]:
                        alive += 1
                    if surface[i][j + 1]:
                        alive += 1
                    if surface[0][j - 1]:
                        alive += 1
                    if surface[0][j]:
                        alive += 1
                    if surface[0][j + 1]:
                        alive += 1

            else:

                if j == 0:
                    if surface[i - 1][mesh_width - 1]:
                        alive += 1
                    if surface[i - 1][j]:
                        alive += 1
                    if surface[i - 1][j + 1]:
                        alive += 1
                    if surface[i][mesh_width - 1]:
                        alive += 1
                    if surface[i][j + 1]:
                        alive += 1
                    if surface[i + 1][mesh_width - 1]:
                        alive += 1
                    if surface[i + 1][j]:
                        alive += 1
                    if surface[i + 1][j + 1]:
                        alive += 1

                elif j == mesh_width - 1:
                    if surface[i - 1][j - 1]:
                        alive += 1
                    if surface[i - 1][j]:
                        alive += 1
                    if surface[i - 1][0]:
                        alive += 1
                    if surface[i][j - 1]:
                        alive += 1
                    if surface[i][0]:
                        alive += 1
                    if surface[i + 1][j - 1]:
                        alive += 1
                    if surface[i + 1][j]:
                        alive += 1
                    if surface[i + 1][0]:
                        alive += 1

                else:

                    if surface[i - 1][j - 1]:
                        alive += 1
                    if surface[i - 1][j]:
                        alive += 1
                    if surface[i - 1][j + 1]:
                        alive += 1
                    if surface[i][j - 1]:
                        alive += 1
                    if surface[i][j + 1]:
                        alive += 1
                    if surface[i + 1][j - 1]:
                        alive += 1
                    if surface[i + 1][j]:
                        alive += 1
                    if surface[i + 1][j + 1]:
                        alive += 1

            if surface[i][j]:
                if alive < 2:
                    new_surface[i][j] = False
                elif 2 <= alive <= 3:
                    new_surface[i][j] = True
                else:
                    new_surface[i][j] = False
            else:
                if alive == 3:
                    new_surface[i][j] = True

    return new_surface
