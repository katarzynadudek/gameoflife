import threading
from math import floor
from random import randint

from kivy.app import App
from kivy.core.window import Window
from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Rectangle, Line
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget

from game_of_life import calculate_next_step


def create_mesh(size, iteration):
    wid.canvas.clear()

    height = Window.height
    width = Window.width

    if (iteration > size):

        stepWidth = floor((0.7 * width) / (iteration + 1))
        stepHeight = floor(0.9 * height / (iteration + 1))

    else:
        stepWidth = floor((0.7 * width) / (size + 1))
        stepHeight = floor(0.9 * height / (size + 1))

    with wid.canvas:
        Color(1., 1, 1)

        for index in range(0, iteration + 1):
            # poziome
            Line(points=[0.1 * width,
                         0.9 * height - stepHeight * index,
                         size * stepWidth + 0.1 * width,
                         0.9 * height - stepHeight * index],
                 width=1)

        for index in range(0, size + 1):
            # pionowe
            Line(points=[(size - index) * stepWidth + 0.1 * width,
                         0.9 * height,
                         (size - index) * stepWidth + 0.1 * width,
                         0.9 * height - (stepHeight * iteration)],
                 width=1)

    pass


class Board(Widget):

    def __init__(self, **kwargs):
        super(Board, self).__init__(**kwargs)
        self.bind(on_touch_down=self.touch_down)

        self.register_event_type('on_test')

    def touch_down(self, _, touch):
        self.dispatch('on_test', touch)
        print(touch)

    def on_test(self, touch):
        pass


class NewClass(FloatLayout):

    def __init__(self, **kwargs):
        super(NewClass, self).__init__(**kwargs)

        self.speed = 1.0
        self.mesh_height = int(1)
        self.mesh_width = int(1)
        self.callback_cout = 1
        self.animation_length = 100
        Window.size = (800, 650)

        staticButton = Button(text='niezmienne', size_hint_x=None, size_hint=(.15, .09),
                              background_color=(1, 0, 0, 1), pos_hint={'x': 0.05, 'y': 1.85})
        staticButton.bind(on_press=lambda x: self.draw_state(state='static'))
        self.add_widget(staticButton)

        gliderButton = Button(text='glider', size_hint_x=None, size_hint=(.15, .09),
                              background_color=(1, 0, 0, 1), pos_hint={'x': 0.2, 'y': 1.85})
        gliderButton.bind(on_press=lambda x: self.draw_state(state='glider'))
        self.add_widget(gliderButton)

        oscillatorButton = Button(text='oscylator', size_hint_x=None, size_hint=(.15, .09),
                                  background_color=(1, 0, 0, 1), pos_hint={'x': 0.35, 'y': 1.85})
        oscillatorButton.bind(on_press=lambda x: self.draw_state(state='oscillator'))
        self.add_widget(oscillatorButton)

        randomButton = Button(text='losowy', size_hint_x=None, size_hint=(.15, .09),
                              background_color=(1, 0, 0, 1), pos_hint={'x': 0.5, 'y': 1.85})
        randomButton.bind(on_press=lambda x: self.draw_state(state='random'))
        self.add_widget(randomButton)

        userInputButton = Button(text='reczna definicja', size_hint_x=None, size_hint=(.15, .09),
                                 background_color=(1, 0, 0, 1), pos_hint={'x': 0.65, 'y': 1.85})
        userInputButton.bind(on_press=lambda x: self.draw_state(state='user_input'))
        self.add_widget(userInputButton)

        label = Label(text='Rozmiar siatki: ', pos_hint={'x': 0.1, 'y': .1}, size_hint=(.15, .09))
        self.add_widget(label)

        heightInput = TextInput(text='', size_hint=(.05, .09), pos_hint={'x': 0.25, 'y': .1}, multiline=False)
        heightInput.bind(text=lambda x, y: self.save_width(mesh_width=heightInput.text))
        self.add_widget(heightInput)

        widthInput = TextInput(text='', size_hint=(.05, .09), pos_hint={'x': 0.3, 'y': .1}, multiline=False)
        widthInput.bind(text=lambda x, y: self.save_height(mesh_height=widthInput.text))
        self.add_widget(widthInput)

        label = Label(text='Ile ruchow: ', pos_hint={'x': 0.45, 'y': .1}, size_hint=(.15, .09))
        self.add_widget(label)

        animation_length = TextInput(text='', size_hint=(.05, .09), pos_hint={'x': 0.6, 'y': .1}, multiline=False)
        animation_length.bind(text=lambda x, y: self.save_animation_length(animation_length=animation_length.text))
        self.add_widget(animation_length)

        animation = Button(text='START', size_hint=(.15, .09),
                           background_color=(1, 0, 0, 1), pos_hint={'x': 0.7, 'y': .1})
        animation.bind(on_press=self.call_animation)
        self.add_widget(animation)

        speed_up = Button(text='>>', size_hint=(.05, .09),
                          background_color=(1, 0, 0, 1), pos_hint={'x': 0.87, 'y': .1})
        speed_up.bind(on_press=self.speed_up)
        self.add_widget(speed_up)

        board = Board(pos_hint={'x': .5, 'y': .5}, size_hint=(.2, .1))
        board.bind(on_test=self.on_board_event)
        self.add_widget(board)

    def speed_up(self, *args):
        self.speed = 0.1
        self.cancel_animation()
        self.animate()

    def on_board_event(self, _, touch):

        stepWidth = floor((0.7 * Window.width) / (self.mesh_width + 1))
        stepHeight = floor(0.9 * Window.height / (self.mesh_width + 1))

        print(0.1 * Window.width + stepWidth * self.mesh_width)
        print(0.1 * Window.width)
        print(0.9 * Window.height)
        print(0.9 * Window.height - (
                stepHeight * self.mesh_height))

        if 0.1 * Window.width + stepWidth * self.mesh_width >= touch.x >= 0.1 * Window.width and 0.9 * Window.height >= touch.y >= 0.9 * Window.height - (
                stepHeight * self.mesh_height):
            print('called')
            print(int((touch.x - 0.1 * Window.width) / stepWidth))
            print(self.mesh_height - int(
                (touch.y - (0.9 * Window.height - (stepHeight * self.mesh_height))) / stepHeight))
            self.draw_point(int((touch.x - 0.1 * Window.width) / stepWidth), int(
                self.mesh_height - int(touch.y - (0.9 * Window.height - (stepHeight * self.mesh_height))) / stepHeight))

    def call_animation(self, button):

        if (button.text == 'START'):
            self.animate()
            button.text = 'STOP'
        else:
            self.cancel_animation()
            button.text = 'START'

    def animate(self, *args):

        self.t = threading.Timer(self.speed, self.animate)
        self.t.start()

        self.surface = calculate_next_step(self.surface, self.mesh_height, self.mesh_width)

        wid.canvas.clear()
        create_mesh(self.mesh_width, self.mesh_height)
        self.callback_cout += 1

        for i in range(self.mesh_height):
            for j in range(self.mesh_width):
                if self.surface[i][j]:
                    self.draw_point(j, i)

        if self.callback_cout == self.animation_length:
            self.cancel_animation()

    def cancel_animation(self):
        if hasattr(self, 't'):
            self.t.cancel()

    def draw_point(self, index_x, index_y):

        self.surface[int(index_y)][int(index_x)] = True

        stepWidth = floor((0.7 * Window.width) / (self.mesh_width + 1))
        stepHeight = floor(0.9 * Window.height / (self.mesh_width + 1))

        with wid.canvas:
            Color(1, 1, 1, 1)
            Rectangle(
                pos=(0.1 * Window.width + (stepWidth * index_x), 0.9 * Window.height - (stepHeight * (index_y + 1))),
                size=(stepWidth, stepWidth))

    def draw_state(self, state):
        if not hasattr(self, 'mesh_height') or self.mesh_height < 3:
            pass
        else:
            wid.canvas.clear()
            create_mesh(self.mesh_width, self.mesh_height)
            self.surface = [[False for x in range(self.mesh_width)] for y in range(self.mesh_height)]
            if state == 'static':
                self.draw_point(index_x=self.mesh_width / 2, index_y=self.mesh_height / 2 - 1)
                self.draw_point(index_x=self.mesh_width / 2 - 1, index_y=self.mesh_height / 2 - 1)
                self.draw_point(index_x=self.mesh_width / 2 - 2, index_y=self.mesh_height / 2)
                self.draw_point(index_x=self.mesh_width / 2 + 1, index_y=self.mesh_height / 2)
                self.draw_point(index_x=self.mesh_width / 2, index_y=self.mesh_height / 2 + 1)
                self.draw_point(index_x=self.mesh_width / 2 - 1, index_y=self.mesh_height / 2 + 1)
                pass
            if state == 'oscillator':
                self.draw_point(index_x=self.mesh_width / 2, index_y=self.mesh_height / 2)
                self.draw_point(index_x=self.mesh_width / 2, index_y=self.mesh_height / 2 - 1)
                self.draw_point(index_x=self.mesh_width / 2, index_y=self.mesh_height / 2 - 2)
                pass
            if state == 'random':
                for i in range(int((self.mesh_width * self.mesh_height) / 5)):
                    self.draw_point(index_x=randint(0, self.mesh_width - 1), index_y=randint(0, self.mesh_height - 1))

            if state == 'glider':
                self.draw_point(index_x=self.mesh_width / 2, index_y=self.mesh_height / 2)
                self.draw_point(index_x=self.mesh_width / 2, index_y=self.mesh_height / 2 - 1)
                self.draw_point(index_x=self.mesh_width / 2 + 1, index_y=self.mesh_height / 2)
                self.draw_point(index_x=self.mesh_width / 2 + 1, index_y=self.mesh_height / 2 + 1)
                self.draw_point(index_x=self.mesh_width / 2 - 1, index_y=self.mesh_height / 2 + 1)
                pass

    def save_animation_length(self, animation_length):

        if animation_length != '':
            self.animation_length = int(animation_length)
            # create_mesh(self.mesh_width, self.mesh_height)

    def save_width(self, mesh_width):
        if mesh_width != '':
            self.mesh_width = int(mesh_width)
            # create_mesh(self.mesh_width, self.mesh_height)

    def save_height(self, mesh_height):
        if mesh_height != '':
            self.mesh_height = int(mesh_height)
            self.surface = [[False for x in range(self.mesh_width)] for y in range(self.mesh_height)]
            create_mesh(self.mesh_width, self.mesh_height)


class CellularAutomatonApp(App):

    def build(self):
        root = BoxLayout(orientation='vertical')
        root.add_widget(wid)
        root.add_widget(NewClass())
        return root


if __name__ == '__main__':
    wid = Widget()
    CellularAutomatonApp().run()
